import Settings from '../resources/settings.js'

export const create = (req, res) => {
    const settings = Settings.create(req.body)

    return res.status(200).json(settings)
}

export const list = (req, res) => {
    const setting = Settings.list();

    return res.status(200).json(setting)
}

export const find = (req, res) => {
    const settings = Settings.find(req.params.id);

    return res.status(200).json(settings)
}

export const remove = (req, res) => {
    const settings = Settings.remove(req.params.id);

    return res.status(200).json(settings)
}

export const update = (req, res) => {
    const settings = Settings.update(req.params.id);

    return res.status(200).json(settings)
}