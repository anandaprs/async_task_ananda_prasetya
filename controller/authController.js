import Auth from '../resources/auth.js'

export const login = (req, res) => {
    const authentification = Auth.login(req.body)

    return res.status(200).json(authentification)
}

export const logout = (req, res) => {
    const authentification = Auth.logout(req.body)

    return res.status(200).json(authentification)
}

export const verification = (req, res) => {
    const authentification = Auth.verification(req.body)

    return res.status(200).json(authentification)
}
