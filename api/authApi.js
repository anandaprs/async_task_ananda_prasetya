import express from "express";
import { login, logout, verification } from "../controller/authController.js";

const router = express.Router();

router.post('/auth/login', login);
router.post('/auth/logout', logout);
router.post('/auth/verification', verification);

export default router