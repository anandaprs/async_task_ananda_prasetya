import express from "express";
import { create, list, find, remove, update } from "../controller/settingsController.js";

const router = express.Router();

router.get('/settings/users', list); // GET
router.post('/settings/user/:id', create); //POST
router.get('/settings/user/:id', find); // GET
router.delete('/settings/user/:id', remove); // DELETE
router.put('/settings/user/:id', update); // UPDATE

export default router