const auth = []

class Auth {
    constructor(params)
    {
        this.id = params.id;
        this.user = params.user;
        this.password = params.password;
    }

    static login(params)
    {
        const authentification = new this(params)

        auth.push(authentification)

        return authentification;
    }

    static logout(params)
    {
        const authentification = new this(params)

        auth.push(authentification)

        return authentification;
    }

    static verification(params)
    {
        const authentification = new this(params)

        auth.push(authentification)

        return authentification;
    }

}

export default Auth