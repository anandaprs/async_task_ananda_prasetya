const setting = []

class Settings {
    constructor(params)
    {
        this.id = params.id;
        this.account = params.account;
        this.display = params.display;
        this.language = params.language;
        this.about = params.about
    }

    static list()
    {
        return setting;
    }

    static create(params)
    {
        const settings = new this(params)

        setting.push(settings)

        return settings;
    }

    static find(id)
    {
        const settings = setting.find((i) => i.id === Number(id));
        if(!settings)
            return null;
        
        return settings;
    }

    static remove(id)
    {
        const settings = setting.remove((i) => i.id === Number(id));
        if(!settings)
            return null;
        
        return settings;
    }

    static update(id)
    {
        const settings = setting.update((i) => i.id === Number(id));
        if(!settings)
            return null;
        
        return settings;
    }
}

export default Settings